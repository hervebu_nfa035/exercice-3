ATTENTION :
Si vous avez choisi d'effectuer une inscription totalement en ligne et
 que vous avez déjà fourni les pièces justificatives. 
 Merci de nous renvoyer dater et signer le récapitulatif d'inscription 
 qui vous a été envoyé par mail à l'adresse e-mail 
inscription@cnam-iledefrance.fr. 
Sinon, merci de suivre les instructions indiquées dans l'e-mail récapitulatif pour l'envoi des pièces justificatives.