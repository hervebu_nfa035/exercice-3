package fr.cnam.foad.nfa035.fileutils.streaming.testLog4j;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
/**
 *  Test serialisation par flux avec traces utilisant log4j
 * @author herve
 *
 */
public class StreamingTestLog4J {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
	        
			
        	org.apache.logging.log4j.Logger log = LogManager.getLogger();
            File image = new File("petite_image_2.png");
            ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

            // Sérialisation
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(image, media);

            String encodedImage = media.getEncodedImageOutput().toString();
           
            log.info(encodedImage);
            // Désérialisation
            ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

            deserializer.deserialize(media);
            byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
            // Vérification
            // 1/ Automatique
            byte[] originImage = Files.readAllBytes(image.toPath());
            assert Arrays.equals(originImage, deserializedImage);
            // System.out.println("Cette sérialisation est bien réversible :)");
            log.debug("Cette serialisation est bien r�versible ");
            
            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            log.warn("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");
            System.out.println("x");
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addAppender() {
		// TODO Auto-generated method stub
		
	}

	public static String difference(String str1, String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str1;
        }
        int at = indexOfDifference(str1, str2);
        if (at == -1) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return -1;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }
        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); ++i) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }
        return -1;
    }
}


