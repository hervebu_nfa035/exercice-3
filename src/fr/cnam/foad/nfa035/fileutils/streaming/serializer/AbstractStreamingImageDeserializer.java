package fr.cnam.foad.nfa035.fileutils.streaming.serializer;

import java.io.IOException;

/**
 * Classe abstraite visant à structurer/guider le developpement de maniere rigoureuse
 * Elle s'applique donc à tout objet dont les methodes auraient pour effet la s�rialisation d'une image,
 * quel qu'en soit le format ou le media/canal de destination.
 *
 * @param <M> Le Media de serialisation,
 *           à partir duquel nous voulons deserialiser notre image en base 64,
 *           c'est-à-dire la recuperer à son format original.
 */
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {

    /**
     * Désérialise une image depuis un media quelconque vers un support quelconque
     *
     * @param media
     * @throws IOException
     */
    @Override
    public final void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}
