package fr.cnam.foad.nfa035.fileutils.streaming.Junit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import junit.framework.TestCase;
/**
 * Classe contenant des test unitaires (Junit) pour tester la s�rialisation par flux  
 * 
 */
public class StreamingTestJunit extends TestCase {
/**
 * 
 * @param filename
 * @return byte array correspondant � la deserialisation de l'image serialis�e.
 * @throws Exception
 */
	public byte[] deserialiseSerialisedImage(String filename) throws Exception {
		File image = new File(filename);
        ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

        // Sérialisation
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);

        String encodedImage = media.getEncodedImageOutput().toString();
        // System.out.println(encodedImage + "\n");

        // Désérialisation
        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

        deserializer.deserialize(media);
        return  ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
	}
	/**
	 * 
	 * @param filename
	 * @return byte array correspondant au contenu du fichier
	 * @throws Exception
	 */
	public byte[] getByteOf (String filename) throws Exception {
		File image = new File(filename);
		return Files.readAllBytes(image.toPath());
	}
	/**
	 *  test unitaire (Junit) pour tester la s�rialisation par flux de petite_image_2.png
	 * @throws Exception : Exception lev�e si le r�sultat de la d�s�rialisation de l'image s�rialis�e est diff�rent de l'image originale
	 */
	public void testStreamingSerialiser() throws Exception  {

		String filename = "petite_image_2.png";
        byte[] deserializedImage = deserialiseSerialisedImage(filename);
       
        byte[] originImage = getByteOf(filename);

        System.out.println(" deserializedImage length " + deserializedImage.length + " originImage length " + originImage.length);
        
		assertEquals (deserializedImage.length,originImage.length);
		
		for (int ind = 0; ind < deserializedImage.length; ind++ )
		{
			assertEquals (deserializedImage[ind],originImage[ind]);
		}
	
	}

	/**
	 *  test unitaire (Junit) pour tester la s�rialisation par flux de superman.jpg
	 * @throws Exception : Exception lev�e si le r�sultat de la d�s�rialisation de l'image s�rialis�e est diff�rent de l'image originale
	 */
	public void testStreamingSerialiser2() throws Exception  {
		String filename = "superman.jpg";
		byte[] deserializedImage = deserialiseSerialisedImage(filename);
   
		byte[] originImage = getByteOf(filename);       

        System.out.println(" deserializedImage length " + deserializedImage.length + " originImage length " + originImage.length);
        // end
		assertEquals (deserializedImage.length,originImage.length);
		
		for (int ind = 0; ind < deserializedImage.length; ind++ )
		{
			assertEquals (deserializedImage[ind],originImage[ind]);
		}
	
	}

	/**
	 *  test unitaire (Junit) pour tester la s�rialisation par flux de Capture_StreamingTestJunit.jpg
	 * @throws Exception : Exception lev�e si le r�sultat de la d�s�rialisation de l'image s�rialis�e est diff�rent de l'image originale
	 */
	public void testStreamingSerialiser3() throws Exception  {
		String filename = "Capture_StreamingTestJunit.png";
		byte[] deserializedImage = deserialiseSerialisedImage(filename);

		byte[] originImage = getByteOf(filename);       

        System.out.println(" deserializedImage length " + deserializedImage.length + " originImage length " + originImage.length);
        // end
		assertEquals (deserializedImage.length,originImage.length);
		
		for (int ind = 0; ind < deserializedImage.length; ind++ )
		{
			assertEquals (deserializedImage[ind],originImage[ind]);
		}
	
	}
	/**
	 *  test unitaire (Junit) pour tester la s�rialisation par flux de petite_image.jpg
	 * @throws Exception : Exception lev�e si le r�sultat de la d�s�rialisation de l'image s�rialis�e est diff�rent de l'image originale
	 */
	public void testStreamingSerialiser4() throws Exception  {

    
    String filename = "petite_image.png";
    byte[] deserializedImage = deserialiseSerialisedImage(filename);
    
    byte[] originImage = getByteOf(filename);
    
    System.out.println(" deserializedImage length " + deserializedImage.length + " originImage length " + originImage.length);
    
	assertEquals (deserializedImage.length,originImage.length);
	
	for (int ind = 0; ind < deserializedImage.length; ind++ )
	{
		assertEquals (deserializedImage[ind],originImage[ind]);
	}

}
}
