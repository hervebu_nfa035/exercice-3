package fr.cnam.foad.nfa035.fileutils.simpleaccess.junit;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;

import org.junit.Test;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import junit.framework.TestCase;

public class SimpleAccessTestJunit extends TestCase{

	@Test
	
	public void testSimpleAccessSerialiser() throws Exception  {
		File image = new File("petite_image.png");
        ImageSerializer serializer = new ImageSerializerBase64Impl();

        // Sérialization
        String encodedImage = (String) serializer.serialize(image);
        

        // Désérialisation
        byte[] deserializedImage = (byte[]) serializer.deserialize(encodedImage);
        byte[] initialImage = Files.readAllBytes(image.toPath());
        System.out.println(" deserializedImage length " + deserializedImage.length + " initialImage length " + initialImage.length);
        // Vérifications
        //  1/ Automatique
        //assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
		assertEquals (deserializedImage.length,initialImage.length);
		
		for (int ind = 0; ind < deserializedImage.length; ind++ )
		{
			assertEquals (deserializedImage[ind],initialImage[ind]);
		}
	}
}
